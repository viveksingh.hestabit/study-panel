<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', 'Auth\LoginController@showLoginForm')->name('logins');

Route::get('login', 'Auth\LoginController@showLoginForm')->name('login');
Route::post('login', 'Auth\LoginController@login')->name('login');
Route::post('logout', 'Auth\LoginController@logout')->name('logout');

// Registration Routes...
Route::get('register', 'Auth\RegisterController@showRegistrationForm')->name('register');
Route::post('register', 'Auth\RegisterController@create')->name('registersubmit');

// Auth::routes();
Route::middleware(['auth:web'])->group(function () {
    Route::patch('/fcm-token', 'HomeController@updateToken')->name('fcmToken');
    Route::get('/home', 'HomeController@index')->name('home');
    Route::get('/edit-profile', 'HomeController@editProfile')->name('editProfile');
    Route::post('/update-profile', 'HomeController@updateProfile')->name('updateProfile');
});
