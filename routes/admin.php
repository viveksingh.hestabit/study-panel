<?php

use Illuminate\Support\Facades\Route;

Route::prefix('admin')->name('admin.')->namespace('Auth')->middleware('guest:admin')->group(function () {
    // Authentication Routes...
    Route::get('/', 'LoginController@showLoginForm')->name('login');
    Route::get('login', 'LoginController@showLoginForm')->name('login');
    Route::post('login', 'LoginController@login')->name('login.submit');
});

Route::name('admin.')->prefix('admin')->middleware(['auth:admin'])->group(function () {
    Route::patch('/fcm-token', 'HomeController@updateToken')->name('fcmToken');
    Route::post('logout', 'Auth\LoginController@logout')->name('logout');
    Route::get('editprofile', 'HomeController@editprofile')->name('editprofile');
    Route::post('updateprofile', 'HomeController@updateprofile')->name('updateprofile');


    Route::get('/home', 'HomeController@index')->name('home');
    Route::group(['prefix' => 'teacher'], function () {
        Route::get('/', 'TeacherController@index')->name('techers');
        Route::get('/update/{id}', 'TeacherController@update')->name('teachers.update');
        Route::post('/delete/{id}', 'TeacherController@destroy')->name('teachers.delete');
    });

    Route::group(['prefix' => 'user'], function () {
        Route::get('/', 'UserController@index')->name('users');
        Route::post('/delete/{id}', 'UserController@destroy')->name('users.delete');
        Route::get('/edit/{id}', 'UserController@edit')->name('users.edit');
        Route::get('/update/{id}', 'UserController@update')->name('users.update');
        Route::post('/assign/teacher/{id}', 'UserController@assignteacher')->name('users.assignteacher');
    });
});
