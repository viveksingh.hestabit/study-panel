<?php

use Illuminate\Support\Facades\Route;

Route::name('teacher.')->prefix('teacher')->namespace('Auth')->middleware('guest:teacher')->group(function () {

    // Authentication Routes...
    Route::get('/', 'LoginController@showLoginForm')->name('login');
    Route::get('login', 'LoginController@showLoginForm')->name('login');
    Route::post('login', 'LoginController@login')->name('loginsubmit');

    // Registration Routes...
    Route::get('register', 'RegisterController@showRegistrationForm')->name('register');
    Route::post('register', 'RegisterController@create')->name('register');
});

Route::name('teacher.')->prefix('teacher')->middleware(['auth:teacher'])->group(function () {
    Route::patch('/fcm-token', 'HomeController@updateToken')->name('fcmToken');
    Route::post('logout', 'Auth\LoginController@logout')->name('logout');

    Route::get('/home', 'HomeController@index')->name('home');
    Route::get('/students', 'HomeController@students')->name('students');
    Route::get('/edit-profile', 'HomeController@editProfile')->name('editProfile');
    Route::post('/update-profile', 'HomeController@updateProfile')->name('updateProfile');
});
