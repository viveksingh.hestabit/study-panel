<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;


Route::group(['prefix' => 'admin'], function () {

    Route::post('login', 'LoginController@login');
    Route::post('token/refresh', 'LoginController@refreshToken');

    Route::group(['middleware' => ['auth:admin-api']], function () {
        Route::apiResource('students', StudentController::class);
        Route::apiResource('teachers', TeacherController::class);
        Route::post('teachers/approve', 'TeacherController@approve');
        Route::post('students/approve', 'StudentController@approve');
        Route::post('students/assign', 'StudentController@assign');
    });
});
