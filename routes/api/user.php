<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

Route::group(['prefix' => 'user'], function () {

    Route::post('login', 'LoginController@login');
    Route::post('signUp', 'LoginController@signup');
    Route::post('token/refresh', 'LoginController@refreshToken');

    Route::group(['middleware' => ['auth:user']], function () {
        Route::post('updateprofile/detail', 'LoginController@update_profile');
        Route::get('getprofile', 'LoginController@get_profile');
    });
});
