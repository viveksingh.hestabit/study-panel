<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StudentSignupRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [

            'name'  =>  'required|string|max:255',
            'email' => 'required|email|unique:users,email',
            'password' =>  'required|min:8',
            'address'  => 'required|string',
            'current_school'  => 'required|string',
            'previous_school'  => 'required|string',
            'parent_detail'  => 'required|string',
            'profile_image'  => 'required|mimes:jpg,png',
        ];
    }
}
