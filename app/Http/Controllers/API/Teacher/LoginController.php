<?php

namespace App\Http\Controllers\API\Teacher;


use App\Models\User;
use App\Models\UserDetail;
use Illuminate\Http\Request;
use App\Models\TeacherDetail;
use Illuminate\Support\Facades\DB;
use App\Http\Requests\LoginRequest;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Http;
use App\Http\Resources\TeacherResource;
use Illuminate\Support\Facades\Storage;
use App\Http\Requests\RefreshTokenRequest;
use App\Http\Requests\TeacherSignupRequest;
use App\Http\Requests\UpdateTeacherProfileRequest;

class LoginController extends Controller
{
    public function get_profile(Request $request)
    {
        $teacher = User::find(auth()->user()->id);
        if ($teacher) {
            return res_success('Success!',  new TeacherResource($teacher));
        } else {
            return res_failed('Data Not Found!');
        }
    }

    public function signup(TeacherSignupRequest $request)
    {
        $teacher = new User();
        $teacher->name = $request->name;
        $teacher->email = $request->email;
        $teacher->password = bcrypt($request->password);
        $teacher->user_type = 'teacher';
        if ($teacher->save()) {
            if ($request->has('fcm_token')) {
                $teacher->update(['fcm_token' => $request->fcm_token]);
            }
            $backupLoc = 'public/Image/';
            if (!is_dir($backupLoc)) {
                Storage::makeDirectory($backupLoc, 0755, true, true);
            }
            $userdetail = new  UserDetail();
            $userdetail->user_id = $teacher->id;
            if ($request->has('profile_image')) {
                $file = $request->file('profile_image');
                $profile_image = time() . '_' . $file->getClientOriginalName();
                $upload_success1 = $request->file('profile_image')->storeAs('public/Image', $profile_image);
                $uploaded_profile_image = 'Image/' . $profile_image;
                $userdetail->profile =  $uploaded_profile_image;
            }
            $userdetail->address = $request->address;
            $userdetail->current_school = $request->current_school;
            $userdetail->previous_school = $request->previous_school;
            $userdetail->save();

            $teacherdetail = new  TeacherDetail();
            $teacherdetail->user_id = $teacher->id;
            $teacherdetail->experience = $request->experience;
            $teacherdetail->expertise_subject = $request->expertise_subject;
            $teacherdetail->save();
        }

        $teacher = User::find($teacher->id);
        $token = $this->_token($request->email, $request->password);
        if (isset($token['access_token'])) {
            return res_success('Success!', [
                'teacher'       =>  new TeacherResource($teacher),
                'token_type'    =>  $token['token_type'],
                'expires_in'    =>  $token['expires_in'],
                'access_token'  =>  $token['access_token'],
                'refresh_token' =>  $token['refresh_token'],
            ], 201);
        }
        return res(500, 'Please generate clients first!');
    }

    public function update_profile(UpdateTeacherProfileRequest $request)
    {
        $user = User::find(auth()->user()->id);
        if ($user) {
            $user->name =  $request->name;
            $user->save();
        }
        $userdetail = UserDetail::where('user_id', auth()->user()->id)->first();
        if ($userdetail) {
            $userdetail->address =  $request->address;

            $backupLoc = 'public/Image/';
            if (!is_dir($backupLoc)) {
                Storage::makeDirectory($backupLoc, 0755, true, true);
            }
            if ($request->has('profile_image')) {
                // Image delete
                $filePath = $userdetail->profile;
                if ($filePath != null) {
                    $filePath1 = storage_path('app/public/' . $filePath);
                    if (is_file($filePath1)) {
                        unlink($filePath1);
                    }
                }
                $file = $request->file('profile_image');
                $profile_image = time() . '_' . $file->getClientOriginalName();
                $upload_success1 = $request->file('profile_image')->storeAs('public/Image', $profile_image);
                $uploaded_profile_image = 'Image/' . $profile_image;
                $userdetail->profile =  $uploaded_profile_image;
            }
            $userdetail->current_school =  $request->current_school;
            $userdetail->previous_school =  $request->previous_school;
            $userdetail->save();
        }
        $teacherdetail = TeacherDetail::where('user_id', auth()->user()->id)->first();
        if ($teacherdetail) {
            $teacherdetail->experience = $request->experience;
            $teacherdetail->expertise_subject = $request->expertise_subject;
            $teacherdetail->save();
        }

        return res_success('Success!',  ['userdata' => new TeacherResource($user)]);
    }


    public function login(LoginRequest $request)
    {
        $teacher = User::where('email', $request->email)->first();
        if ($teacher) {
            if (Hash::check($request->password, $teacher->password)) {
                if ($request->has('fcm_token')) {
                    $teacher->update(['fcm_token' => $request->fcm_token]);
                }
                $token = $this->_token($request->email, $request->password);
                if (isset($token['access_token'])) {
                    return res_success('Success!', [
                        'teacher'       =>  new TeacherResource($teacher),
                        'token_type'    =>  $token['token_type'],
                        'expires_in'    =>  $token['expires_in'],
                        'access_token'  =>  $token['access_token'],
                        'refresh_token' =>  $token['refresh_token'],
                    ]);
                }
                return res(500, 'Server Error!');
            }
            return res_failed('Invalid password!');
        }
        return res_failed('Invalid user!');
    }

    public function refreshToken(RefreshTokenRequest $request)
    {
        $token = $this->_tokenRefresh($request->refresh_token);
        if (isset($token['access_token'])) {
            return res_success('Success!', [
                'token_type'    =>  $token['token_type'],
                'expires_in'    =>  $token['expires_in'],
                'access_token'  =>  $token['access_token'],
                'refresh_token' =>  $token['refresh_token'],
            ]);
        } elseif (count($token)) {
            return res_failed($token['message']);
        }
        return res(500, 'Server Error!');
    }

    private function _token($username, $password)
    {
        if ($client = $this->_client()) {
            $response = Http::asForm()->post(url('oauth/token'), [
                'grant_type' => 'password',
                'provider' => 'teachers',
                'client_id' => $client->id,
                'client_secret' => $client->secret,
                'username' => $username,
                'password' => $password,
                // 'scope' => '',
            ]);
            return $response->json();
        }
        return [];
    }
    private function _tokenRefresh($refresh_token)
    {
        if ($client = $this->_client()) {
            $response = Http::asForm()->post(url('oauth/token'), [
                'grant_type' => 'refresh_token',
                'provider' => 'teachers',
                'client_id' => $client->id,
                'client_secret' => $client->secret,
                'refresh_token' => $refresh_token,
                // 'scope' => '',
            ]);
            return $response->json();
        }
        return [];
    }
    private function _client()
    {
        return DB::table('oauth_clients')
            ->where('provider', 'teachers')
            ->where('password_client', 1)
            ->first();
    }
}
