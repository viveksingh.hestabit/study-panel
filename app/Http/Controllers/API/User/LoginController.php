<?php

namespace App\Http\Controllers\API\User;

use App\Models\User;
use App\Models\UserDetail;
use Illuminate\Http\Request;
use App\Models\StudentDetail;
use Illuminate\Support\Facades\DB;
use App\Http\Requests\LoginRequest;
use App\Http\Controllers\Controller;
use App\Http\Resources\UserResource;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use App\Http\Requests\RefreshTokenRequest;
use App\Http\Requests\StudentSignupRequest;
use App\Http\Requests\UpdateStudentProfileRequest;

class LoginController extends Controller
{
    public function get_profile(Request $request)
    {
        $user = User::find(auth()->user()->id);
        if ($user) {
            return res_success('Success!', new UserResource($user));
        } else {
            return res_failed('Data Not Found!');
        }
    }

    public function signup(StudentSignupRequest $request)
    {
        $student = new User();
        $student->name = $request->name;
        $student->email = $request->email;
        $student->password = bcrypt($request->password);
        $student->user_type = 'student';
        if ($student->save()) {
            if ($request->has('fcm_token')) {
                $student->update(['fcm_token' => $request->fcm_token]);
            }
            $backupLoc = 'public/Image/';
            if (!is_dir($backupLoc)) {
                Storage::makeDirectory($backupLoc, 0755, true, true);
            }
            $userdetail = new  UserDetail();
            $userdetail->user_id = $student->id;
            if ($request->has('profile_image')) {
                $file = $request->file('profile_image');
                $profile_image = time() . '_' . $file->getClientOriginalName();
                $upload_success1 = $request->file('profile_image')->storeAs('public/Image', $profile_image);
                $uploaded_profile_image = 'Image/' . $profile_image;
                $userdetail->profile =  $uploaded_profile_image;
            }
            $userdetail->address = $request->address;
            $userdetail->current_school = $request->current_school;
            $userdetail->previous_school = $request->previous_school;
            $userdetail->save();

            $studentdetail = new  StudentDetail();
            $studentdetail->user_id = $student->id;
            $studentdetail->parent_detail = $request->parent_detail;
            $studentdetail->save();
        }

        $student = User::find($student->id);
        $token = $this->_token($request->email, $request->password);
        if (isset($token['access_token'])) {
            return res_success('Success!', [
                'user'       =>  new UserResource($student),
                'token_type'    =>  $token['token_type'],
                'expires_in'    =>  $token['expires_in'],
                'access_token'  =>  $token['access_token'],
                'refresh_token' =>  $token['refresh_token'],
            ], 201);
        }
        return res(500, 'Please generate clients first!');
    }

    public function login(LoginRequest $request)
    {
        $user = User::where('email', $request->email)
            ->first();

        if ($user) {
            if (Hash::check($request->password, $user->password)) {
                if ($request->has('fcm_token')) {
                    $user->update(['fcm_token' => $request->fcm_token]);
                }
                $token = $this->_token($user->email, $request->password);
                if (isset($token['access_token'])) {
                    return res_success('Success!', [
                        'user'          =>  new UserResource($user),
                        'token_type'    =>  $token['token_type'],
                        'expires_in'    =>  $token['expires_in'],
                        'access_token'  =>  $token['access_token'],
                        'refresh_token' =>  $token['refresh_token'],
                    ]);
                }
                return res(500, 'Server Error!');
            }
            return res_failed('Invalid password!');
        }
        return res_failed('Invalid user!');
    }

    public function update_profile(UpdateStudentProfileRequest $request)
    {
        $user = User::find(auth()->user()->id);
        if ($user) {
            $user->name =  $request->name;
            $user->save();
        }
        $userdetail = UserDetail::where('user_id', auth()->user()->id)->first();
        if ($userdetail) {
            $userdetail->address =  $request->address;

            $backupLoc = 'public/Image/';
            if (!is_dir($backupLoc)) {
                Storage::makeDirectory($backupLoc, 0755, true, true);
            }
            if ($request->has('profile_image')) {
                // Image delete
                $filePath = $userdetail->profile;
                if ($filePath != null) {
                    $filePath1 = storage_path('app/public/' . $filePath);
                    if (is_file($filePath1)) {
                        unlink($filePath1);
                    }
                }
                $file = $request->file('profile_image');
                $profile_image = time() . '_' . $file->getClientOriginalName();
                $upload_success1 = $request->file('profile_image')->storeAs('public/Image', $profile_image);
                $uploaded_profile_image = 'Image/' . $profile_image;
                $userdetail->profile =  $uploaded_profile_image;
            }
            $userdetail->current_school =  $request->current_school;
            $userdetail->previous_school =  $request->previous_school;
            $userdetail->save();
        }
        $StudentDetail = StudentDetail::where('user_id', auth()->user()->id)->first();
        if ($StudentDetail) {
            $StudentDetail->parent_detail =  $request->parent_detail;
            $StudentDetail->save();
        }

        return res_success('Success!',  ['userdata' => new UserResource($user)]);
    }


    public function refreshToken(RefreshTokenRequest $request)
    {

        $token = $this->_tokenRefresh($request->refresh_token);

        if (isset($token['access_token'])) {
            return res_success('Success!', [
                'token_type'    =>  $token['token_type'],
                'expires_in'    =>  $token['expires_in'],
                'access_token'  =>  $token['access_token'],
                'refresh_token' =>  $token['refresh_token'],
            ]);
        } elseif (count($token)) {
            return res_failed($token['message']);
        }
        return res(500, 'Server Error!');
    }

    private function _token($username, $password)
    {
        if ($client = $this->_client()) {
            $response = Http::asForm()->post(url('oauth/token'), [
                'grant_type' => 'password',
                'provider' => 'users',
                'client_id' => $client->id,
                'client_secret' => $client->secret,
                'username' => $username,
                'password' => $password,
                // 'scope' => '',
            ]);

            return $response->json();
        }
        return [];
    }

    private function _tokenRefresh($refresh_token)
    {
        if ($client = $this->_client()) {
            $response = Http::asForm()->post(url('oauth/token'), [
                'grant_type' => 'refresh_token',
                'provider' => 'users',
                'client_id' => $client->id,
                'client_secret' => $client->secret,
                'refresh_token' => $refresh_token,
                // 'scope' => '',
            ]);

            return $response->json();
        }
        return [];
    }

    private function _client()
    {
        return DB::table('oauth_clients')
            ->where('provider', 'users')
            ->where('password_client', 1)
            ->first();
    }
}
