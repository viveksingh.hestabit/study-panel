<?php

namespace App\Http\Controllers\API\Admin;

use Exception;
use App\Models\User;
use App\Models\UserDetail;
use Illuminate\Http\Request;
use App\Models\TeacherDetail;
use App\Http\Requests\ValidateUser;
use Illuminate\Support\Facades\Log;
use App\Http\Controllers\Controller;
use App\Notifications\appNotification;
use App\Http\Resources\TeacherResource;
use Illuminate\Support\Facades\Storage;
use App\Http\Requests\TeacherSignupRequest;
use App\Http\Requests\UpdateTeacherProfileRequest;
use App\Notifications\accountApprovalNotification;

class TeacherController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = User::where('user_type', 'teacher')->get();
        $data = [];
        if (count($users) > 0) {
            foreach ($users as $user) {
                $data[] =  new TeacherResource($user);
            }
            return res_success('Success!', $data);
        } else {
            return res_failed('Data Not Found!');
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(TeacherSignupRequest $request)
    {
        $student = new User();
        $student->name = $request->name;
        $student->email = $request->email;
        $student->password = bcrypt($request->password);
        $student->user_type = 'teacher';
        $student->active = 1;
        if ($student->save()) {
            $backupLoc = 'public/Image/';
            if (!is_dir($backupLoc)) {
                Storage::makeDirectory($backupLoc, 0755, true, true);
            }


            $userdetail = new  UserDetail();
            $userdetail->user_id = $student->id;
            if ($request->has('profile_image')) {
                $file = $request->file('profile_image');
                $profile_image = time() . '_' . $file->getClientOriginalName();
                $upload_success1 = $request->file('profile_image')->storeAs('public/Image', $profile_image);
                $uploaded_profile_image = 'Image/' . $profile_image;
                $userdetail->profile =  $uploaded_profile_image;
            }
            $userdetail->address = $request->address;
            $userdetail->current_school = $request->current_school;
            $userdetail->previous_school = $request->previous_school;
            $userdetail->save();

            $teacherdetail = new  TeacherDetail();
            $teacherdetail->user_id = $student->id;
            $teacherdetail->experience = $request->experience;
            $teacherdetail->expertise_subject = $request->expertise_subject;
            $teacherdetail->save();
        }

        $student = User::find($student->id);
        return res_success('Success!', ['userdata' =>  new TeacherResource($student)]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user = User::where('user_type', 'teacher')->where('id', $id)->first();
        if ($user) {
            return res_success('Success!', new TeacherResource($user));
        } else {
            return res_failed('Data Not Found!');
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateTeacherProfileRequest $request, $id)
    {
        // dd($request->all());
        $user = User::where('user_type', 'teacher')->where('id', $id)->first();
        if ($user) {
            $user->name =  $request->name;
            $user->save();

            $userdetail = UserDetail::where('user_id', $id)->first();
            if ($userdetail) {
                $userdetail->address =  $request->address;

                $backupLoc = 'public/Image/';
                if (!is_dir($backupLoc)) {
                    Storage::makeDirectory($backupLoc, 0755, true, true);
                }
                if ($request->has('profile_image')) {
                    // Image delete
                    $filePath = $userdetail->profile;
                    if ($filePath != null) {
                        $filePath1 = storage_path('app/public/' . $filePath);
                        if (is_file($filePath1)) {
                            unlink($filePath1);
                        }
                    }
                    $file = $request->file('profile_image');
                    $profile_image = time() . '_' . $file->getClientOriginalName();
                    $upload_success1 = $request->file('profile_image')->storeAs('public/Image', $profile_image);
                    $uploaded_profile_image = 'Image/' . $profile_image;
                    $userdetail->profile =  $uploaded_profile_image;
                }
                $userdetail->current_school =  $request->current_school;
                $userdetail->previous_school =  $request->previous_school;
                $userdetail->save();
            }
            $teacherdetail = TeacherDetail::where('user_id', $id)->first();
            if ($teacherdetail) {
                $teacherdetail->experience = $request->experience;
                $teacherdetail->expertise_subject = $request->expertise_subject;
                $teacherdetail->save();
            }
            return res_success('Success!',  ['userdata' => new TeacherResource($user)]);
        }
        return res_failed('Data Not Found!');
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = User::findOrFail($id);
        if ($user) {
            $user->delete();
            return res_success('Teacher Deleted!');
        } else {
            return res_failed('Data Not Found!');
        }
    }

    public function approve(ValidateUser $request)
    {
        $teacher = User::find($request->user_id);
        if ($teacher->active == 1) {
            return res_success('Already Approved');
        } else {
            try {
                $teacher->notify(new accountApprovalNotification);
                $title = 'Account Approved!';
                $message = 'Your Account Approved By the Admin.';
                // FcmService::send([$teacher->fcm_token],['title' => $title,'body' => $message]);
                $teacher->notify(new appNotification($title, $message, [$teacher->fcm_token]));
            } catch (Exception $e) {
                Log::error($e);
            }
            $teacher->active = 1;
        }
        $teacher->save();
        return res_success('Teacher Approved Successfully!');
    }
}
