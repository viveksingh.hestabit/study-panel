<?php

namespace App\Http\Controllers\API\Admin;

use Exception;
use App\Models\User;
use App\Models\UserDetail;
use App\Services\FcmService;
use Illuminate\Http\Request;
use App\Models\StudentDetail;
use App\Models\StudentTeacher;
use App\Http\Requests\ValidateUser;
use Illuminate\Support\Facades\Log;
use App\Http\Controllers\Controller;
use App\Http\Resources\UserResource;
use App\Notifications\appNotification;
use Illuminate\Support\Facades\Storage;
use App\Http\Requests\AssignTeacherRequest;
use App\Http\Requests\StudentSignupRequest;
use App\Http\Requests\UpdateStudentProfileRequest;
use App\Notifications\accountApprovalNotification;
use App\Notifications\newStudentAssignedNotification;

class StudentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = User::where('user_type', 'student')->get();
        $data = [];
        if (count($users) > 0) {
            foreach ($users as $user) {
                $data[] =  new UserResource($user);
            }
            return res_success('Success!', $data);
        } else {
            return res_failed('Data Not Found!');
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StudentSignupRequest $request)
    {
        $student = new User();
        $student->name = $request->name;
        $student->email = $request->email;
        $student->password = bcrypt($request->password);
        $student->user_type = 'student';
        $student->active = 1;
        if ($student->save()) {
            $backupLoc = 'public/Image/';
            if (!is_dir($backupLoc)) {
                Storage::makeDirectory($backupLoc, 0755, true, true);
            }


            $userdetail = new  UserDetail();
            $userdetail->user_id = $student->id;
            if ($request->has('profile_image')) {
                $file = $request->file('profile_image');
                $profile_image = time() . '_' . $file->getClientOriginalName();
                $upload_success1 = $request->file('profile_image')->storeAs('public/Image', $profile_image);
                $uploaded_profile_image = 'Image/' . $profile_image;
                $userdetail->profile =  $uploaded_profile_image;
            }
            $userdetail->address = $request->address;
            $userdetail->current_school = $request->current_school;
            $userdetail->previous_school = $request->previous_school;
            $userdetail->save();

            $studentdetail = new  StudentDetail();
            $studentdetail->user_id = $student->id;
            $studentdetail->parent_detail = $request->parent_detail;
            $studentdetail->save();
        }

        $student = User::find($student->id);
        return res_success('Success!', ['userdata' =>  new UserResource($student)]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user = User::where('user_type', 'student')->where('id', $id)->first();

        if ($user) {
            return res_success('Success!', new UserResource($user));
        } else {
            return res_failed('Data Not Found!');
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateStudentProfileRequest $request, $id)
    {
        $user = User::where('user_type', 'student')->where('id', $id)->first();
        if ($user) {
            $user->name =  $request->name;
            $user->save();

            $userdetail = UserDetail::where('user_id', $id)->first();
            if ($userdetail) {
                $userdetail->address =  $request->address;

                $backupLoc = 'public/Image/';
                if (!is_dir($backupLoc)) {
                    Storage::makeDirectory($backupLoc, 0755, true, true);
                }
                if ($request->has('profile_image')) {
                    // Image delete
                    $filePath = $userdetail->profile;
                    if ($filePath != null) {
                        $filePath1 = storage_path('app/public/' . $filePath);
                        if (is_file($filePath1)) {
                            unlink($filePath1);
                        }
                    }
                    $file = $request->file('profile_image');
                    $profile_image = time() . '_' . $file->getClientOriginalName();
                    $upload_success1 = $request->file('profile_image')->storeAs('public/Image', $profile_image);
                    $uploaded_profile_image = 'Image/' . $profile_image;
                    $userdetail->profile =  $uploaded_profile_image;
                }
                $userdetail->current_school =  $request->current_school;
                $userdetail->previous_school =  $request->previous_school;
                $userdetail->save();
            }
            $StudentDetail = StudentDetail::where('user_id', $id)->first();
            if ($StudentDetail) {
                $StudentDetail->parent_detail =  $request->parent_detail;
                $StudentDetail->save();
            }

            return res_success('Success!',  ['userdata' => new UserResource($user)]);
        }
        return res_failed('Data Not Found!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = User::findOrFail($id);
        if ($user) {
            $user->delete();
            return res_success('Student Deleted!');
        } else {
            return res_failed('Data Not Found!');
        }
    }

    public function approve(ValidateUser $request)
    {
        $user = User::find($request->user_id);
        if ($user->active == 1) {
            return res_success('Already Approved');
        } else {
            try {
                $user->notify(new accountApprovalNotification);
                $title = 'Account Approved!';
                $message = 'Your Account Approved By the Admin.';
                // FcmService::send([$teacher->fcm_token],['title' => $title,'body' => $message]);
                $user->notify(new appNotification($title, $message, [$user->fcm_token]));
            } catch (Exception $e) {
                Log::error($e);
            }
            $user->active = 1;
        }
        $user->save();
        return res_success('User Approved Successfully!');
    }

    public function assign(AssignTeacherRequest $request)
    {
        $count = StudentTeacher::where(['teacher_id' => $request->teacher_id, 'user_id' => $request->user_id])->count();
        if ($count == 0) {
            $user = new StudentTeacher();
            $user->teacher_id = $request->teacher_id;
            $user->user_id = $request->user_id;
            if ($user->save()) {
                $teacher = User::find($request->teacher_id);
                try {
                    $teacher->notify(new newStudentAssignedNotification);
                    $title = 'New Student Assigned';
                    $message = 'A new Student Assigned To You';
                    // FcmService::send([$teacher->fcm_token],['title' => $title,'body' => $message]);
                    $teacher->notify(new appNotification($title, $message, [$teacher->fcm_token]));
                } catch (Exception $e) {
                    dd($e);
                    Log::error($e);
                }
                return res_success('Teacher Assigned Successfully!!');
            }
        } else {
            return res_success('Teacher Already Assigned!!');
        }
    }
}
