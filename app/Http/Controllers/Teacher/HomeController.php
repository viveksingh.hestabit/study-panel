<?php

namespace App\Http\Controllers\Teacher;

use Carbon\Carbon;

use App\Models\User;
use App\Models\UserDetail;
use Illuminate\Http\Request;
use App\Models\TeacherDetail;
use App\Http\Controllers\Controller;
use App\Models\StudentTeacher;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\ValidationException;

class HomeController extends Controller
{
    public function index()
    {
        $data['user'] = User::find(auth()->user()->id);
        return view('teacher.home',$data);
    }
    public function students()
    {
        $data['users'] = StudentTeacher::where('teacher_id',auth()->user()->id)->get();
        return view('teacher.users',$data);
    }


    public function editProfile()
    {
        $data['user'] = User::find(auth()->user()->id);
        return view('teacher.edit',$data);
    }



    public function updateProfile(Request $request)
    {

        $validator=Validator::make($request->all(),[
            'name' => 'required|string|max:255',
            'address'  => 'required|string',
            'profile_image'=>'sometimes|nullable|max:2000',
            'current_school'  => 'required|string',
            'previous_school'  => 'required|string',
            'experience'  => 'required|numeric|gt:0',
            'expertise_subject'  => 'required|string',
        ]);
        if($validator->fails())
        {
            return redirect()->back()->withErrors($validator->errors());
        }


        $backupLoc='public/Image/';
        if(!is_dir($backupLoc)) {
           Storage::makeDirectory($backupLoc, 0755, true, true);
        }
        $user= User::where('id',Auth::user()->id)->first();
        $user->name =  $request->name;
        $user->save();

        $userdetail= UserDetail::where('user_id',auth()->user()->id)->first();
        if( $userdetail!=null)
        {
            if($request->has('profile_image'))
            {
                // Image delete
                $filePath = $userdetail->profile;

                if($filePath != null)
                {
                   $filePath1 = storage_path('app/public/'. $filePath);
                   if(is_file($filePath1))
                   {
                      unlink($filePath1);
                   }
                }
                $file = $request->file('profile_image');
                $profile_image = time().'_'.$file->getClientOriginalName();
                $upload_success1 = $request->file('profile_image')->storeAs('public/Image',$profile_image);
                $uploaded_profile_image = 'Image/'.$profile_image;
                $userdetail->profile =  $uploaded_profile_image;
            }
            $userdetail->address =  $request->address;
            $userdetail->current_school =  $request->current_school;
            $userdetail->previous_school =  $request->previous_school;
            $userdetail->save();
        }
        else
        {
            $userdetail = new UserDetail();
            if($request->has('profile_image'))
            {
                $file = $request->file('profile_image');
                $profile_image = time().'_'.$file->getClientOriginalName();
                $upload_success1 = $request->file('profile_image')->storeAs('public/Image',$profile_image);
                $uploaded_profile_image = 'Image/'.$profile_image;
                $userdetail->profile =  $uploaded_profile_image;
            }
            $userdetail->user_id = auth()->user()->id;
            $userdetail->address =  $request->address;
            $userdetail->current_school =  $request->current_school;
            $userdetail->previous_school =  $request->previous_school;
            $userdetail->save();
        }

        $teacherdetail= TeacherDetail::where('user_id',auth()->user()->id)->first();
        if( $teacherdetail!=null)
        {
            $teacherdetail->experience =  $request->experience;
            $teacherdetail->expertise_subject =  $request->expertise_subject;
            $teacherdetail->save();
        }
        else
        {
            $teacherdetail = new TeacherDetail();
            $teacherdetail->user_id = auth()->user()->id;
            $teacherdetail->experience =  $request->experience;
            $teacherdetail->expertise_subject =  $request->expertise_subject;
            $teacherdetail->save();
        }

        return redirect()->route('teacher.home')->with('status','Profile Updated!');
    }

    public function updateToken(Request $request){
        try{
            $request->user()->update(['fcm_token'=>$request->token]);
            return response()->json([
                'success'=>true
            ]);
        }catch(\Exception $e){
            report($e);
            return response()->json([
                'success'=>false
            ],500);
        }
    }

}
