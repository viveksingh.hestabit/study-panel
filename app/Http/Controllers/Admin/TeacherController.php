<?php

namespace App\Http\Controllers\Admin;

use Exception;
use App\Models\User;
use App\Services\FcmService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use App\Http\Controllers\Controller;
use App\Notifications\appNotification;
use App\Notifications\accountApprovalNotification;

class TeacherController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['teachers'] = User::where('user_type', 'teacher')->get();
        return view('admin.teachers', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $teacher = User::find($id);
        if ($teacher->active == 1) {
            $teacher->active = 0;
        } else {
            try {
                $teacher->notify(new accountApprovalNotification);
                $title = 'Account Approved!';
                $message = 'Your Account Approved By the Admin.';
                // FcmService::send($teacher->fcm_token, ['title' => $title, 'body' => $message]);
                $teacher->notify(new appNotification($title, $message, $teacher->fcm_token));
            } catch (Exception $e) {
                Log::error($e);
            }
            $teacher->active = 1;
        }
        $teacher->save();
        return redirect()->back()->with('status', 'Status updated successfully!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
