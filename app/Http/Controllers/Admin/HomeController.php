<?php

namespace App\Http\Controllers\Admin;

use App\Models\User;
use App\Services\FcmService;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use App\Notifications\appNotification;
use Illuminate\Support\Facades\Validator;
use App\Notifications\accountApprovalNotification;

class HomeController extends Controller
{
    public function index()
    {
        $teacher = User::find(2);
        $teacher->notify(new accountApprovalNotification);
        $title = 'Account Approved!';
        $message = 'Your Account Approved By the Admin.';
        $teacher->notify(new appNotification($title, $message, [$teacher->fcm_token]));
        dd(FcmService::send($teacher->fcm_token, ['title' => $title, 'body' => $message]));
        $data['user'] = User::find(auth()->user()->id);
        return view('admin.home', $data);
    }

    public function editprofile()
    {
        $data['user'] = User::find(auth()->user()->id);
        return view('admin.edit', $data);
    }

    public function updateProfile(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|string|max:255',
            'email'  => 'required|email',
        ]);
        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator->errors());
        }

        $user = User::where('id', auth()->user()->id)->first();
        $user->name =  $request->name;
        $user->email =  $request->email;
        $user->save();


        return redirect()->route('admin.home')->with('status', 'Profile Updated!');
    }

    public function updateToken(Request $request)
    {
        try {
            $request->user()->update(['fcm_token' => $request->token]);
            return response()->json([
                'success' => true
            ]);
        } catch (\Exception $e) {
            report($e);
            return response()->json([
                'success' => false
            ], 500);
        }
    }
}
