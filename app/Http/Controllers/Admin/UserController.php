<?php

namespace App\Http\Controllers\Admin;

use Exception;
use App\Models\User;
use App\Services\FcmService;
use Illuminate\Http\Request;
use App\Models\StudentTeacher;
use Illuminate\Support\Facades\Log;
use App\Http\Controllers\Controller;
use App\Notifications\appNotification;
use Illuminate\Support\Facades\Validator;
use App\Notifications\accountApprovalNotification;
use App\Notifications\newStudentAssignedNotification;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['users'] =  User::where('user_type', 'student')->get();
        return view('admin.users', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['teachers'] = User::where('user_type', 'teacher')->where('active', 1)->get();
        $data['user'] = User::find($id);
        return view('admin.update-user', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function assignteacher(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'teacher_id' => 'required|numeric|not_in:0|exists:users,id',
            'user_id' => 'required|exists:users,id',
        ]);
        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator->errors());
        }
        $count = StudentTeacher::where(['teacher_id' => $request->teacher_id, 'user_id' => $request->user_id])->count();
        if ($count == 0) {
            $user = new StudentTeacher();
            $user->teacher_id = $request->teacher_id;
            $user->user_id = $request->user_id;
            if ($user->save()) {
                $teacher = User::find($request->teacher_id);
                try {
                    $teacher->notify(new newStudentAssignedNotification);
                    $title = 'New Student Assigned';
                    $message = 'A new Student Assigned To You';
                    FcmService::send([$teacher->fcm_token], ['title' => $title, 'body' => $message]);
                    $teacher->notify(new appNotification($title, $message, [$teacher->fcm_token]));
                } catch (Exception $e) {
                    Log::error($e);
                }
            }
            return redirect(route('admin.users'))->with('status', 'Teacher Assigned Successfully!!');
        } else {
            return redirect(route('admin.users'))->with('status', 'Teacher Already Successfully!!');
        }
    }

    public function update(Request $request, $id)
    {
        $user = User::find($id);
        if ($user->active == 1) {
            $user->active = 0;
        } else {
            try {
                $user->notify(new accountApprovalNotification);
                $title = 'Account Approved!';
                $message = 'Your Account Approved By the Admin.';
                FcmService::send($user->fcm_token, ['title' => $title, 'body' => $message]);
                $user->notify(new appNotification($title, $message, [$user->fcm_token]));
            } catch (Exception $e) {
                Log::error($e);
            }
            $user->active = 1;
        }
        $user->save();
        return redirect()->back()->with('status', 'Status updated successfully!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
