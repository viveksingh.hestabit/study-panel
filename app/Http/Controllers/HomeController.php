<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Models\UserDetail;
use VivekPassword\Password;
use App\Services\FcmService;
use Illuminate\Http\Request;
use App\Models\StudentDetail;

use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Crypt;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Notification;
use App\Notifications\newStudentAssignedNotification;
use Vivek\UniqueId;

class HomeController extends Controller
{

    public function index()
    {
        // print(Password::generate(9));
        // dd(UniqueId::generate());
        $data['user'] = User::find(auth()->user()->id);
        return view('home',$data);
    }

    public function editProfile()
    {
        $data['user'] = User::find(auth()->user()->id);
        return view('edit',$data);
    }



    public function updateProfile(Request $request)
    {
        
        $validator=Validator::make($request->all(),[
            'name' => 'required|string|max:255',
            'address'  => 'required|string',
            'profile_image'=>'sometimes|nullable|max:2000',
            'current_school'  => 'required|string',
            'previous_school'  => 'required|string',
            'parent_detail'  => 'required|string',
        ]);
        if($validator->fails())
        {
            return redirect()->back()->withErrors($validator->errors());
        }

        $backupLoc='public/Image/';
        if(!is_dir($backupLoc)) {
           Storage::makeDirectory($backupLoc, 0755, true, true);
        }
        $user= User::where('id',Auth::user()->id)->first();
        $user->name =  $request->name;
        $user->save();

        $userdetail= UserDetail::where('user_id',auth()->user()->id)->first();
        if( $userdetail!=null)
        {
            if($request->has('profile_image'))
            {
                // Image delete
                $filePath = $userdetail->profile;
 
                if($filePath != null)
                {
                   $filePath1 = storage_path('app/public/'. $filePath);
                   if(is_file($filePath1))
                   {
                      unlink($filePath1);
                   }
                }
                $file = $request->file('profile_image');
                $profile_image = time().'_'.$file->getClientOriginalName();
                // $upload_success1 = $request->file('profile_image')->storeAs('public/Image',$profile_image);
                
                $destinationPath = storage_path('app/public/Image');
                $imgFile = Image::make($file->getRealPath());
                $imgFile->resize(150, 150,
                    // function ($constraint) {$constraint->aspectRatio();}
                )->save($destinationPath.'/'.$profile_image);
                $uploaded_profile_image = 'Image/'.$profile_image;
                $userdetail->profile =  $uploaded_profile_image;
            }
            $userdetail->address =  $request->address;
            $userdetail->current_school =  $request->current_school;
            $userdetail->previous_school =  $request->previous_school;
            $userdetail->save();
        }
        else
        {
            $userdetail = new UserDetail();
            if($request->has('profile_image'))
            {
                $file = $request->file('profile_image');
                $profile_image = time().'_'.$file->getClientOriginalName();
                $upload_success1 = $request->file('profile_image')->storeAs('public/Image',$profile_image);
                $uploaded_profile_image = 'Image/'.$profile_image;
                $userdetail->profile =  $uploaded_profile_image;
            }
            $userdetail->user_id = auth()->user()->id;
            $userdetail->address =  $request->address;
            $userdetail->current_school =  $request->current_school;
            $userdetail->previous_school =  $request->previous_school;
            $userdetail->save();
        }

        $studentdetail= StudentDetail::where('user_id',auth()->user()->id)->first();
        if( $studentdetail!=null)
        {
            $studentdetail->parent_detail =  $request->parent_detail;
            $studentdetail->save();
        }
        else
        {
            $studentdetail = new StudentDetail();
            $studentdetail->user_id = auth()->user()->id;
            $studentdetail->parent_detail =  $request->parent_detail;
            $studentdetail->save();
        }

        return redirect()->route('home')->with('status','Profile Updated!');
    }

    public function updateToken(Request $request){
        try{
            $request->user()->update(['fcm_token'=>$request->token]);
            return response()->json([
                'success'=>true
            ]);
        }catch(\Exception $e){
            report($e);
            return response()->json([
                'success'=>false
            ],500);
        }
    }

}
