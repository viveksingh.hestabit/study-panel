<?php

use Illuminate\Support\Facades\Storage;

if (!function_exists('res_success')) {
    function res_success($message = 'Success!', $data = [])
    {
        return res(200, $message, $data);
    }
}

if (!function_exists('res_failed')) {
    function res_failed($message = 'Failed!', $data = [])
    {
        return res(422, $message, $data);
    }
}

if (!function_exists('res')) {
    function res($status_code, $message, $data = [])
    {
        return response()->json([
            'status'    =>  $status_code,
            'message'   =>  $message,
            'result'    =>  (object) $data
        ]);
    }
}
