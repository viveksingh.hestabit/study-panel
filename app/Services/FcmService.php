<?php

namespace App\Services;

use Illuminate\Support\Facades\Http;

class FCMService
{
    public static function send($token, $notification)
    {
        Http::acceptJson()->withToken(config('fcm.token'))->post(
            'https://fcm.googleapis.com/fcm/send',
            [
                'to' => $token,
                'notification' => $notification,
            ]
        );

        // Http::acceptJson()->post(
        //     'https://onesignal.com/api/v1/notifications',
        //     [
        //         'app_id' => '6638f579-a44c-4aec-bbad-550a06c58e77',
        //         'include_player_ids' => [$token],
        //         'contents' => ['en' => $notification['body']],
        //         'headings' => ['en' => $notification['title']],
        //         'data' => $notification,
        //         'content_available' => 1,
        //     ]
        // );
        
    }
}
