importScripts('https://www.gstatic.com/firebasejs/8.3.2/firebase-app.js');
importScripts('https://www.gstatic.com/firebasejs/8.3.2/firebase-messaging.js');
   
firebase.initializeApp({
    apiKey: "AIzaSyDfEvVg5WX0WH0kXm2qFpKZr3qlZaU1Ww4",
    authDomain: "study-panel.firebaseapp.com",
    projectId: "study-panel",
    storageBucket: "study-panel.appspot.com",
    messagingSenderId: "490655242648",
    appId: "1:490655242648:web:ca34c5419c1ced852873ed",
    measurementId: "G-W1L4PWW01V"
});
  
const messaging = firebase.messaging();
messaging.setBackgroundMessageHandler(function({data:{title,body,icon}}) {
    return self.registration.showNotification(title,{body,icon});
});