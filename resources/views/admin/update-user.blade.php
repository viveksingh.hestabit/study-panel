@extends('admin.layouts.admin')

@section('content')

<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Update User</div>

                <div class="card-body">
                    <form method="POST" action="{{ route('admin.users.assignteacher',$user->id) }}" >
                        @csrf

                        <div class="row mb-3">
                            <label for="user_id" class="col-md-4 col-form-label text-md-end">Student</label>
                            <div class="col-md-6">
                                <input type="hidden" name="user_id" value="{{$user->id}}"/>

                                <input type="text" class="form-control @error('user_id') is-invalid @enderror" name="name" value="{{$user->name}}"/>
                            </div>
                            <div class="col-md-6">

                                @error('user_id')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>


                        <div class="row mb-3">
                            <label for="teacher_id" class="col-md-4 col-form-label text-md-end">Select Teacher</label>

                            <div class="col-md-6">
                                <select class="form-control @error('teacher_id') is-invalid @enderror" name="teacher_id" id="">
                                    <option value="0">Select Teacher</option>
                                    @forelse ($teachers as $teacher)
                                        <option @if ($user->teacher_id==$teacher->id) selected @endif value="{{$teacher->id}}">{{$teacher->name}}</option>
                                    @empty
                                    @endforelse
                                </select>
                                @error('teacher_id')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>


                        <div class="row mb-0">
                            <div class="col-md-8 offset-md-4">
                                <button type="submit" class="btn btn-primary">Submit</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
