@extends('admin.layouts.admin')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header text-center">All Teachers</div>

                    <div class="card-body">
                        @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                {{ session('status') }}
                            </div>
                        @endif

                        <table id="example" class="table table-striped" style="width:100%">
                            <thead>
                                <tr>
                                    <th>Profile</th>
                                    <th>Name</th>
                                    <th>Email</th>
                                    <th>Address</th>
                                    <th>Current School</th>
                                    <th>Previous School</th>
                                    <th>Experience</th>
                                    <th>Expertise</th>
                                    <th>Students</th>
                                    <th>status</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                @forelse ($teachers as $teacher)
                                    <tr>
                                        <td>
                                            <img src="{{$teacher->getUserDetails!=null ? $teacher->getUserDetails->profile : null}}" style="max-width: 40px;border-radius:50%" alt="">
                                        </td>
                                        <td>{{$teacher->name}}</td>
                                        <td>{{$teacher->email}}</td>
                                        <td>{{$teacher->getUserDetails!=null ? $teacher->getUserDetails->address : null}}</td>
                                        <td>{{$teacher->getUserDetails!=null ? $teacher->getUserDetails->current_school : null}}</td>
                                        <td>{{$teacher->getUserDetails!=null ? $teacher->getUserDetails->previous_school : null}}</td>
                                        <td>{{$teacher->getTeacherDetails!=null ? $teacher->getTeacherDetails->experience : null}}</td>
                                        <td>{{$teacher->getTeacherDetails!=null ? $teacher->getTeacherDetails->expertise_subject : null}}</td>
                                        <td>
                                                @if (count($teacher->getStudent)>0)
                                                    @forelse ($teacher->getStudent as $user)
                                                       {{$user->getUserData->name}},
                                                    @empty
                                                    @endforelse
                                                @endif
                                        </td>
                                        @if ($teacher->active==1)
                                            <td><span class="btn btn-success btn-sm">Active</span></td>
                                        @else
                                            <td><span class="btn btn-danger btn-sm">Inactive</span></td>
                                        @endif
                                        <td><a href="{{route('admin.teachers.update',$teacher->id)}}" class="btn btn-primary btn-sm">Change Status</a>
                                        </td>
                                    </tr>
                                @empty

                                @endforelse
                            </tbody>
                        </table>


                    </div>
                </div>
            </div>
        </div>
    </div>

    @push('script')
        <script>
            $(document).ready(function () {
                $('#example').DataTable();
            });
        </script>
    @endpush
@endsection
