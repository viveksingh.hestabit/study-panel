@extends('admin.layouts.admin')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header text-center">All Users</div>

                    <div class="card-body">
                        @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                {{ session('status') }}
                            </div>
                        @endif

                        <table id="example" class="table table-striped" style="width:100%">
                            <thead>
                                <tr>
                                    <th>Profile</th>
                                    <th>Name</th>
                                    <th>Email</th>
                                    <th>Address</th>
                                    <th>Current School</th>
                                    <th>Previous School</th>
                                    <th>Parent Details</th>
                                    <th>Assign Teacher</th>
                                    <th>status</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                @forelse ($users as $user)
                                    <tr>
                                        <td>
                                            <img src="{{$user->getUserDetails!=null ? $user->getUserDetails->profile : null}}" style="max-width: 40px;border-radius:50%" alt="">
                                        </td>
                                        <td>{{$user->name}}</td>
                                        <td>{{$user->email}}</td>
                                        <td>{{$user->getUserDetails!=null ? $user->getUserDetails->address : null}}</td>
                                        <td>{{$user->getUserDetails!=null ? $user->getUserDetails->current_school : null}}</td>
                                        <td>{{$user->getUserDetails!=null ? $user->getUserDetails->previous_school : null}}</td>
                                        <td>{{$user->getStudentDetails!=null ? $user->getStudentDetails->parent_detail : null}}</td>
                                        <td>{{$user->getTeacher!=null ? $user->getTeacher->getTeacherData->name : null}}</td>

                                        @if ($user->active==1)
                                            <td><span class="btn btn-success btn-sm">Active</span></td>
                                        @else
                                            <td><span class="btn btn-danger btn-sm">Inactive</span></td>
                                        @endif
                                        <td><a href="{{route('admin.users.update',$user->id)}}" class="btn btn-primary btn-sm mb-2">Change Status</a>
                                        <a href="{{route('admin.users.edit',$user->id)}}" class="btn btn-primary btn-sm">Assign Teacher</a></td>
                                    </tr>
                                @empty
                                @endforelse
                            </tbody>
                        </table>


                    </div>
                </div>
            </div>
        </div>
    </div>

    @push('script')
        <script>
            $(document).ready(function () {
                $('#example').DataTable();
            });
        </script>
    @endpush
@endsection
