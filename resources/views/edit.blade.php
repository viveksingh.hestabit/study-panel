@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">Update Profile</div>

                    <div class="card-body">
                        <form method="POST" action="{{ route('updateProfile') }}" enctype="multipart/form-data">
                            @csrf

                            <div class="row mb-3">
                                <label for="name" class="col-md-4 col-form-label text-md-end">Name</label>

                                <div class="col-md-6">
                                    <input id="name" type="text"
                                        class="form-control @error('name') is-invalid @enderror" name="name"
                                        value="{{ $user->name }}" required>

                                    @error('name')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="row mb-3">
                                <label for="profile_image" class="col-md-4 col-form-label text-md-end">Profile Image</label>

                                <div class="col-md-4">
                                    <input id="profile_image" type="file" name="profile_image1"
                                        class="form-control @error('profile_image') is-invalid @enderror" accept="image/*">

                                    @error('profile_image')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                                <div class="col-md-2">
                                    <img src="{{ $user->getUserDetails != null ? $user->getUserDetails->profile : null }}"
                                        style="max-width: 50px;border-radius:50%" alt="">
                                </div>
                            </div>
                            <div class="row mb-3">
                                <label for="address" class="col-md-4 col-form-label text-md-end">Address</label>

                                <div class="col-md-6">
                                    <input id="address" type="text"
                                        class="form-control @error('address') is-invalid @enderror"
                                        value="{{ $user->getUserDetails != null ? $user->getUserDetails->address : null }}"
                                        name="address" required>

                                    @error('address')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="row mb-3">
                                <label for="current_school" class="col-md-4 col-form-label text-md-end">Current
                                    School</label>

                                <div class="col-md-6">
                                    <input id="current_school" type="text"
                                        class="form-control @error('current_school') is-invalid @enderror"
                                        name="current_school"
                                        value="{{ $user->getUserDetails != null ? $user->getUserDetails->current_school : null }}"
                                        required>

                                    @error('current_school')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="row mb-3">
                                <label for="previous_school" class="col-md-4 col-form-label text-md-end">Previous
                                    School</label>

                                <div class="col-md-6">
                                    <input id="previous_school" type="text"
                                        class="form-control @error('previous_school') is-invalid @enderror"
                                        name="previous_school"
                                        value="{{ $user->getUserDetails != null ? $user->getUserDetails->previous_school : null }}"
                                        required>

                                    @error('previous_school')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>


                            <div class="row mb-3">
                                <label for="parent_detail" class="col-md-4 col-form-label text-md-end">Parent Detail</label>

                                <div class="col-md-6">
                                    <textarea type="text" rows="5" class="form-control @error('parent_detail') is-invalid @enderror"
                                        name="parent_detail" required>{{ $user->getStudentDetails != null ? $user->getStudentDetails->parent_detail : null }}</textarea>
                                    @error('parent_detail')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="form-group hirehide is-empty is-fileinput width100">
                                <div class=socialmediaside2>
                                    <input class=fileUpload accept="image/jpeg, image/jpg" name=profile_image[] type=file
                                        value="Choose a file" multiple>
                                    <div class=input-group>
                                        <input class=form-control id=uploadre
                                            placeholder="Please select your profile picture" readonly>
                                        <span class="input-group-btn input-group-sm"><button
                                                class="btn btn-fab btn-fab-mini"type=button><i
                                                    class=material-icons>attach_file</i></button></span>
                                    </div>
                                </div>
                            </div>
                            <div class=upload-demo>
                                <div class="upload-demo-wrap portimg"></div>
                            </div>

                            <div class="row mb-0">
                                <div class="col-md-8 offset-md-4">
                                    <button type="submit" class="btn btn-primary">Submit</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script type="text/javascript" src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
    {{-- <script src="{{ asset('js/img-uploader.js') }}" defer></script> --}}

    <script>
        function readURL() {
            var $input = $(this);
            var $newinput = $('.portimg ');
            for (let index = 0; index < this.files.length; index++) {
                const element = this.files[index];
                if (element) {
                    var reader = new FileReader();
                    reader.onload = function(e) {
                        reset($newinput.next('.delbtn'), true);
                        $newinput.append(`<img alt="your class="img${index}" image" src=${e.target.result}>`);
                        $newinput.append(`<input type="button" data-id="${index}" class="delbtn removebtn" value="remove">`);
                    }
                    reader.readAsDataURL(element);
                }   
            }
            
        }
        $(".fileUpload").change(readURL);
        $("form").on('click', '.delbtn', function(e) {
            reset($(this));
        });

        function reset(elm, prserveFileName) {
            let id = eld.attr('data-id');
            if (elm && elm.length > 0) {
                var $input = elm;
                $input.prev('.portimg').attr('src', '').hide();
                if (!prserveFileName) {
                    $($input).parent().parent().parent().find('input.fileUpload ').val("");
                    //input.fileUpload and input#uploadre both need to empty values for particular div
                }
                elm.remove();
            }
        }
    </script>
@endsection
