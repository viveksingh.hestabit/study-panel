@extends('layouts.app')

@section('content')

<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
                <div class="card">
                    <div class="card-header">{{ __('Dashboard') }} | User</div>

                    <div class="card-body">
                        @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                {{ session('status') }}
                            </div>
                        @endif
                        <div class="row">
                            <img src="{{$user->getUserDetails!=null ? $user->getUserDetails->profile :null}}" style="max-width: 100px;border-radius:50%" alt="">
                        </div>
                        <div class="row">
                            <div class="col-md-4">Name : </div>
                            <div class="col-md-6">{{$user->name}}</div>
                        </div>
                        <div class="row">
                            <div class="col-md-4">Email : </div>
                            <div class="col-md-6">{{$user->email}}</div>
                        </div>
                        <div class="row">
                            <div class="col-md-4">Address : </div>
                            <div class="col-md-6">{{$user->getUserDetails!=null ? $user->getUserDetails->address :null}}</div>
                        </div>
                        <div class="row">
                            <div class="col-md-4">Current school : </div>
                            <div class="col-md-6">{{$user->getUserDetails!=null ?$user->getUserDetails->current_school:null}}</div>
                        </div>
                        <div class="row">
                            <div class="col-md-4">Previous School : </div>
                            <div class="col-md-6">{{$user->getUserDetails!=null ?$user->getUserDetails->previous_school:null}}</div>
                        </div>
                        <div class="row">
                            <div class="col-md-4">Parent Detail : </div>
                            <div class="col-md-6">{{$user->getStudentDetails!=null ? $user->getStudentDetails->parent_detail:null}}</div>
                        </div>
                        {{-- <div class="row">
                            <div class="col-md-4">Teacher</div>
                            <div class="col-md-6">{{$user->getTeacher->name}}</div>
                        </div> --}}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
