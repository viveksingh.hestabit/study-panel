<!DOCTYPE html>
<html lang="en">
<head>
    <title>Unapproved User List</title>
</head>
<body>
    <h1 style="text-align: center">Unapproved Users List</h1>
    <table style="text-align: center;border:1;">
        <tr>
            <th>Sr no. </th>
            <th>Name</th>
        </tr>
            @forelse ($users as $user)
                <tr>
                    <td>{{$loop->iteration}}</td>
                    <td>{{$user}}</td>
                </tr>
            @empty  
            @endforelse
    </table>
</body>
</html>