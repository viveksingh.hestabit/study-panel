@extends('teacher.layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header text-center">All Users</div>

                    <div class="card-body">
                        @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                {{ session('status') }}
                            </div>
                        @endif

                        <table id="example" class="table table-striped" style="width:100%">
                            <thead>
                                <tr>
                                    <th>Name</th>
                                    <th>Email</th>
                                    <th>Address</th>
                                    <th>Current School</th>
                                    <th>Previous School</th>
                                    <th>Parent Details</th>
                                </tr>
                            </thead>
                            <tbody>
                                @forelse ($users as $user)
                                    <tr>
                                        <td>{{$user->getUserData->name}}</td>
                                        <td>{{$user->getUserData->email}}</td>
                                        <td>{{$user->getUserData->getUserDetails!=null ? $user->getUserData->getUserDetails->address : null}}</td>
                                        <td>{{$user->getUserData->getUserDetails!=null ? $user->getUserData->getUserDetails->current_school : null}}</td>
                                        <td>{{$user->getUserData->getUserDetails!=null ? $user->getUserData->getUserDetails->previous_school : null}}</td>
                                        <td>{{$user->getUserData->getUserDetails!=null ? $user->getUserData->getStudentDetails->parent_detail : null}}</td>
                                    </tr>
                                @empty                         
                                @endforelse
                            </tbody>
                        </table>
                        
                        
                    </div>
                </div>
            </div>
        </div>
    </div>
    
    @push('script')
        <script>
            $(document).ready(function () {
                $('#example').DataTable();
            });
        </script>
    @endpush
@endsection
