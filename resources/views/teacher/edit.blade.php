@extends('teacher.layouts.app')

@section('content')

<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Update Profile</div>

                <div class="card-body">
                    <form method="POST" action="{{ route('teacher.updateProfile') }}" enctype="multipart/form-data">
                        @csrf

                        <div class="row mb-3">
                            <label for="name" class="col-md-4 col-form-label text-md-end">Name</label>

                            <div class="col-md-6">
                                <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ $user->name }}" required>

                                @error('name')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                        <div class="row mb-3">
                            <label for="profile_image" class="col-md-4 col-form-label text-md-end">Profile Image</label>

                            <div class="col-md-4">
                                <input id="profile_image" type="file" name="profile_image" class="form-control @error('profile_image') is-invalid @enderror" accept="image/*" >

                                @error('profile_image')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                            <div class="col-md-2">
                                <img src="{{$user->getUserDetails!=null ? $user->getUserDetails->profile :null}}" style="max-width: 50px;border-radius:50%" alt="">
                            </div>
                        </div>
                        <div class="row mb-3">
                            <label for="address" class="col-md-4 col-form-label text-md-end">Address</label>

                            <div class="col-md-6">
                                <input id="address" type="text" class="form-control @error('address') is-invalid @enderror" value="{{$user->getUserDetails!=null ? $user->getUserDetails->address :null}}" name="address" required>

                                @error('address')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="row mb-3">
                            <label for="current_school" class="col-md-4 col-form-label text-md-end">Current School</label>

                            <div class="col-md-6">
                                <input id="current_school" type="text" class="form-control @error('current_school') is-invalid @enderror" name="current_school" value="{{ $user->getUserDetails!=null ? $user->getUserDetails->current_school :null}}" required>

                                @error('current_school')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="row mb-3">
                            <label for="previous_school" class="col-md-4 col-form-label text-md-end">Previous School</label>

                            <div class="col-md-6">
                                <input id="previous_school" type="text" class="form-control @error('previous_school') is-invalid @enderror" name="previous_school" value="{{ $user->getUserDetails!=null ? $user->getUserDetails->previous_school :null}}" required>

                                @error('previous_school')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>


                        <div class="row mb-3">
                            <label for="experience" class="col-md-4 col-form-label text-md-end">Experience</label>

                            <div class="col-md-6">
                                <input type="number" step="any" class="form-control @error('experience') is-invalid @enderror" name="experience" value="{{ $user->getTeacherDetails!=null ? $user->getTeacherDetails->experience :null}}" required>

                                @error('experience')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>


                        <div class="row mb-3">
                            <label for="expertise_subject" class="col-md-4 col-form-label text-md-end">Expertise In Subject</label>

                            <div class="col-md-6">
                                <input id="expertise_subject" type="text" class="form-control @error('expertise_subject') is-invalid @enderror" name="expertise_subject" value="{{ $user->getTeacherDetails!=null ? $user->getTeacherDetails->expertise_subject :null}}" required>

                                @error('expertise_subject')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="row mb-0">
                            <div class="col-md-8 offset-md-4">
                                <button type="submit" class="btn btn-primary">Submit</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
